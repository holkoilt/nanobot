# [[TF2] Nanobot (Dodgeball Bot)](https://forums.alliedmods.net/showthread.php?p=2286346)



## Description:
Hey all, I have decided it is finally time to release this to the public.
This is my version of the custom plugins you see out there for Player vs Bot mode in dodgeball.

## What is Player vs. Bot?
Literally as the name describes it. All players versus one bot that is close to unbeatable (unless specified otherwise via cvar). This mode is popular among dodgeball communities due to it being unique. Furthermore, this mode can be used as practice - to improve you and your players' skills.

## Video Demonstration:
[![[TF2] Nanobot - Dodgeball Bot - Player vs Bot (Demonstration) ](https://img.youtube.com/vi/08lq2MD6knw/0.jpg)](https://www.youtube.com/watch?v=08lq2MD6knw)

## CVARS:
Listed in your cfg/sourcemod/Nanobot.cfg
* **sm_nanobot_version** - Plugin version, no touchy!
* **sm_nanobot_enable [1/0]** - (Default: 1) Enable the plugin? 1 = Yes, 0 = No.
* **sm_nanobot_name "ThisBotRules"** - (Default: "Nanobot") What should be the name of the bot?
* **sm_nanobot_push [0/1/2]** - (Default: 1) Barrier for the bot. 0 = No barrier, 1 = The bot will airblast players away from it, 2 = Slap player away from the bot (deducting 20 health each slap).
* **sm_nanobot_model "models/bots/pyro/bot_pyro.mdl"** - (Default: Pyro Robot from MvM) What should be the player model of Nanobot? Leave this blank if you would prefer the default player model.
* **sm_nanobot_vote_mode [0/1/2/3]** - (Default: 3) Player vs Bot voting. 0 = No voting, 1 = Generic chat vote, 2 = Menu vote, 3 = Both (Generic chat first, then Menu vote).
* **sm_nanobot_vote_time [#]** - (Default: 25.0) Time in seconds the vote menu should last.
* **sm_nanobot_vote_delay [#]** - (Default: 60.0) Time in seconds before players can initiate another PvB vote.
* **sm_nanobot_vote_percentage [0.05-1.0]** - (Default: 0.60) How many players are required for the vote to pass? 0.60 = 60%.
* **sm_nanobot_victory_speed [#]** - (Default: 450) When the rocket reaches greater than or equal to this speed, in MPH, Nanobot will not deflect the rocket and the other team wins. Put this value to 0 if you do not want Nanobot to lose (unbeatable...ish).
* **sm_nanobot_victory_deflects [#]** - (Default: 60) When the total number of deflects reaches greater than or equal to this number, Nanobot will not deflect the rocket and the other team wins. Put this value to 0 if you do not want Nanobot to lose (unbeatable...ish).
* **sm_nanobot_minplayers [#]** - (Default: 1) When there are a minimum of X amount of players or less, enable Nanobot. 0 = No enable, 1 = Enables at 1 player... 10 = Enables at 10 players, etc.
* **sm_nanobot_maxplayers [#]** - (Default: 2) When there are a maximum of X amount of players or more, disable Nanobot. 0 = No disable, 2 = Disables at 2 players... 10 = Disables at >= 10 players, etc.
* **sm_nanobot_size [#.#]** - (Default: 1.0) Change the scale of the bot's model. 1.0 = no scale, 2.0 = 2 times the normal size.
* **sm_nanobot_size_head [#.#]** - (Default: 1.0) Change the scale of the bot's head. 1.0 = no scale, 2.0 = 2 times the normal size.
* **sm_nanobot_size_hand [#.#]** - (Default: 1.0) Change the scale of the bot's head. 1.0 = no scale, 2.0 = 2 times the normal size.
* **sm_nanobot_size_torso [#.#]** - (Default: 1.0) Change the scale of the bot's head. 1.0 = no scale, 2.0 = 2 times the normal size.
* **sm_nanobot_speech [1/0]** - (Default: 1) Should Nanobot antagonize the players when they die? 1 = Yes, 0 = No.
* **sm_nanobot_godmode [1/0]** - (Default: 0) Should Nanobot have godmode? Note that godmode will be revoked when victory speed/deflects is reached causing Nanobot to die. 0 = No, 1 = Yes.

## Commands:
* **sm_nanobot or sm_pvb** - (Default Access: ADMFLAG_RCON) Force enable/disable Player vs Bot mode.
* **sm_autoreflect <name>** - (Default Access: ADMFLAG_CHEATS) Enable/disable auto reflect on a player. If you don't include a player name in the command, then it will enable/disable it on yourself.
* **sm_votepvb** or chat say **votepvb** - (Default Access: Everyone) Vote to enable/disable Player vs Bot mode.

## Want to solo the bot?
Check out my [Arena Solo Mode](https://forums.alliedmods.net/showthread.php?t=261705) plugin.

## Installation:
1. A brain installed to your PCI-Express-brain slot in your head(Between your ears), make sure it's updated to the latest driver and has the "know how to manage a server" module installed.
2. [Install Sourcemod](https://wiki.alliedmods.net/Installing_sourcemod).
3. Extract the zip to your addons/sourcemod/ folder.
4. Restart the server or type "sm plugins load nanobot" in your server console.

## To-Do List:
I am always welcome to suggestions!
* none

## Credits:
* **[ReFlexPoison](https://forums.alliedmods.net/member.php?u=149090)** - I took some calculations and a stock from his Auto Reflect plugin.
* **[ClassicGuzzi](https://forums.alliedmods.net/member.php?u=238864)** - Helped me with fetching the speed of the rocket and converting it to MPH.
